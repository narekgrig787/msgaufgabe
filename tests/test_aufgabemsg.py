import pytest

from aufgabemsg import distance

@pytest.mark.parametrize("input1,input2,input3,expected",
                         [('a', 'c', {('a', 'b'), ('b', 'c')},2),
                          ('a', 'e', {('a', 'b'), ('c', 'b'), ('c', 'd'), ('b', 'd'), ('e', 'd')},3),
                          ('c', 'd', {('a', 'b'), ('b', 'c'), ('c', 'd'), ('b', 'd')},1),
                          ('1', '3', {('1', '2'), ('2', '3')},2),
                          ('1', '5', {('1', '2'), ('3', '2')},-1)
                          ])
def  test_distance(input1,input2,input3,expected):
    result = distance(input1,input2,input3)
    assert result == expected