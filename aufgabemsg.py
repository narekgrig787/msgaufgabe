
from collections import deque
"""
Die Funktion distance() hat folgende 3 Parameter:
 
Ein Ausgangsobjekt
Ein Zielobjekt
Bidirektionale Verbindungen zwischen Objekten als Set/Liste von Objektpaaren
 
Die Funktion distance()  berechnet auf Basis der Verbindungen den kürzesten Weg 
zwischen dem Ausgangsobjekt und dem Zielobjekt und gibt die Länge des Wegs zurück.
 
Beispiele:
 
distance(a, c, {(a,b), (b,c)}) gibt 2 zurück
distance(a, e, {(a,b), (c,b), (c,d), (b,d), (e,d)}) gibt 3 zurück
distance(c, d, {(a,b), (b,c), (c,d), (b,d)}) gibt 1 zurück
Deine Aufgabe:
 
Implementiere die Funktion in einer Programmiersprache deiner Wahl
Füge Kommentare hinzu
Implementiere min. einen Test-Fall mit einem Test-Framework deiner Wahl
Optimiere deine Funktion für mögliche Edge-Cases
Pushe das Code-Projekt auf ein Git-Repo z.B. auf Github
Optional: Erstelle eine CI/CD Pipeline, die den Code automatisch testet

"""

def distance(start,end,connections):

    connectionslist = {} # Leere Set/Liste von Objektpaaren als Verbindung

    # die connectionsliste speichert die nodes und ihre direkte benachbarte nodes auch noch
    for  connection in connections:
        node1, node2 = connection # da wir 2 er paare haben
        if node1 not in connectionslist: # checken ob node bereits in der verbindungs liste ist 
            connectionslist[node1] = []  # falls nicht, node hinzufügen  mit leerer Liste
        if node2 not in connectionslist:
            connectionslist[node2] = []
        # Bidirektionale Verbindungen zwischen Objekten
        connectionslist[node1].append(node2)
        connectionslist[node2].append(node1)
        
    visited = set() # besuchte knoten tracken
    queue = deque([(start, 0)]) # initalisierung der queue  mit startknoten und Startdistanz

    # BFS zu suche der kuerzesten wegs
    # dijkstra wäre auch nutzvoll falls wir gewichte hätten bzw kanten 
    # nicht gleich 1 wären 
    while queue: #  solange es noch Knoten in der Warteschlange gibt
        # node: aktueller knoten
        # dist: Distanz von der aktuellen knoten zum Startknoten 
        node, dist = queue.popleft() # aus queue entnemmen also dequen 
        if node == end:
            return dist # gib distanz zuruck falls ende erreicht
        visited.add(node) # ansonsten als besucht markieren

        for neighbor in connectionslist[node]:  # gehen durch alle benachbarten nodes
            if neighbor not in visited:         # pruefen ob nachbar node bereits besucht wurde
                queue.append((neighbor, dist + 1)) # falls nicht, die nachbar und dist in queue hinzufuegen und dist um 1 erhoehen
    return -1 # falls kein weg zum ziel gib -1 zuruck

if __name__ == "__main__":
    print(distance('a', 'c', {('a', 'b'), ('b', 'c')}))
    print(distance('a', 'e', {('a', 'b'), ('c', 'b'), ('c', 'd'), ('b', 'd'), ('e', 'd')}))
    print(distance('c', 'd', {('a', 'b'), ('b', 'c'), ('c', 'd'), ('b', 'd')}))
    print(distance('1', '3', {('1', '2'), ('2', '3')}))
    print(distance('1', '5', {('1', '2'), ('3', '2')})) # falls nicht geht